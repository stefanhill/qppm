data.processing package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   data.processing.cleaning
   data.processing.custom
   data.processing.filter
   data.processing.pm4py
   data.processing.preprocessing

Submodules
----------

data.processing.AbstractEventLogFilter module
---------------------------------------------

.. automodule:: data.processing.AbstractEventLogFilter
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.AbstractEventLogProcessor module
------------------------------------------------

.. automodule:: data.processing.AbstractEventLogProcessor
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.AbstractFilter module
-------------------------------------

.. automodule:: data.processing.AbstractFilter
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.AbstractProcessor module
----------------------------------------

.. automodule:: data.processing.AbstractProcessor
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.AbstractTraceProcessor module
---------------------------------------------

.. automodule:: data.processing.AbstractTraceProcessor
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: data.processing
   :members:
   :undoc-members:
   :show-inheritance:
