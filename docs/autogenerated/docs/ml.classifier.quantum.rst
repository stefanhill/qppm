ml.classifier.quantum package
=============================

Submodules
----------

ml.classifier.quantum.QuantumEmbedding module
---------------------------------------------

.. automodule:: ml.classifier.quantum.QuantumEmbedding
   :members:
   :undoc-members:
   :show-inheritance:

ml.classifier.quantum.QuantumLayer module
-----------------------------------------

.. automodule:: ml.classifier.quantum.QuantumLayer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ml.classifier.quantum
   :members:
   :undoc-members:
   :show-inheritance:
