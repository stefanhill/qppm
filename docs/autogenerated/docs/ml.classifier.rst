ml.classifier package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ml.classifier.quantum

Submodules
----------

ml.classifier.NGram module
--------------------------

.. automodule:: ml.classifier.NGram
   :members:
   :undoc-members:
   :show-inheritance:

ml.classifier.QKE module
------------------------

.. automodule:: ml.classifier.QKE
   :members:
   :undoc-members:
   :show-inheritance:

ml.classifier.SKWrapper module
------------------------------

.. automodule:: ml.classifier.SKWrapper
   :members:
   :undoc-members:
   :show-inheritance:

ml.classifier.VQC module
------------------------

.. automodule:: ml.classifier.VQC
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ml.classifier
   :members:
   :undoc-members:
   :show-inheritance:
