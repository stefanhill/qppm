data.processing.pm4py package
=============================

Submodules
----------

data.processing.pm4py.LifecycleToName module
--------------------------------------------

.. automodule:: data.processing.pm4py.LifecycleToName
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.ManualRemoveEvents module
-----------------------------------------------

.. automodule:: data.processing.pm4py.ManualRemoveEvents
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.ReduceEndEvents module
--------------------------------------------

.. automodule:: data.processing.pm4py.ReduceEndEvents
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.ReduceLoops module
----------------------------------------

.. automodule:: data.processing.pm4py.ReduceLoops
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.RemoveEventsByIndex module
------------------------------------------------

.. automodule:: data.processing.pm4py.RemoveEventsByIndex
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.RemoveEventsByLifecycle module
----------------------------------------------------

.. automodule:: data.processing.pm4py.RemoveEventsByLifecycle
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.RemoveEventsByName module
-----------------------------------------------

.. automodule:: data.processing.pm4py.RemoveEventsByName
   :members:
   :undoc-members:
   :show-inheritance:

data.processing.pm4py.RenameEvents module
-----------------------------------------

.. automodule:: data.processing.pm4py.RenameEvents
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: data.processing.pm4py
   :members:
   :undoc-members:
   :show-inheritance:
