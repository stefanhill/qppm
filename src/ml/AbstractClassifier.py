import abc
import pickle
import typing
from typing import TYPE_CHECKING

from data.AbstractDataHandler import AbstractDataHandler
from ml import AbstractPrediction
from ml.AbstractMetric import AbstractMetric
from utils.Path import Path

if TYPE_CHECKING:
    from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
    from ml.encoding.BuilderConfiguration import BuilderConfiguration


class AbstractClassifier(abc.ABC):

    def __init__(self,
                 model=None,
                 encoder=None,
                 prediction_type=None):
        self._model = model
        self._encoder: 'EventLogEncodingBuilder' = encoder
        self._prediction_type = prediction_type

    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, value):
        self._model = value

    @property
    def encoder(self) -> 'EventLogEncodingBuilder':
        return self._encoder

    @encoder.setter
    def encoder(self, value: 'EventLogEncodingBuilder'):
        self._encoder = value

    @property
    def prediction_type(self):
        return self._prediction_type

    @prediction_type.setter
    def prediction_type(self, value):
        self._prediction_type = value

    @abc.abstractmethod
    def fit(self, train_handler: 'AbstractDataHandler', val_handler: 'AbstractDataHandler' = None) -> 'AbstractMetric':
        # TODO: no automatic feature label split in here ~ how to tell Camunda?
        pass

    @abc.abstractmethod
    def predict(self, predict_handler: typing.Union['AbstractDataHandler'],
                builder_configuration: 'BuilderConfiguration' = None) -> 'AbstractPrediction':
        pass

    @abc.abstractmethod
    def evaluate(self, val_handler: 'AbstractDataHandler') -> 'AbstractMetric':
        pass

    def is_trained(self) -> bool:
        """Check whether the classifier is already trained

        Although the method is not abstract, implementations might vary

        Returns: True if the classifier is trained

        """
        return self.model is not None

    def save(self, path: typing.Union[str, 'Path']):
        if isinstance(path, Path):
            path = str(path)
        pickle.dump(self, open(path, mode='wb'), protocol=pickle.HIGHEST_PROTOCOL)

    def save_weights(self, path: typing.Union[str, 'Path']):
        if isinstance(path, Path):
            path = str(path)
        pickle.dump(self.model, open(path, mode='wb'), protocol=pickle.HIGHEST_PROTOCOL)

    def load_weights(self, path: typing.Union[str, 'Path']):
        if isinstance(path, Path):
            path = str(path)
        self.model = pickle.load(open(path, mode='rb'))
