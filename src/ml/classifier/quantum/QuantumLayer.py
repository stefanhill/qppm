import pennylane as qml


class QuantumLayer:

    @staticmethod
    def default_variational(w, n_wires=4, n_layers=1):
        i_ = 0
        for w_ in w:
            qml.CRot(w_[0], w_[1], w_[2], wires=[i_ % n_wires, (i_ + 1) % n_wires])