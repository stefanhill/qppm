import typing
from functools import reduce

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils import check_X_y

from ml.metrics.ProbabilisticVectorMetric import ProbabilisticVectorMetric


class NGram(BaseEstimator, ClassifierMixin):

    def __init__(self, feature_delimiter: 'str' = '$', max_classes: int = None, **kwargs):

        """N grams with fixed n

        Args:
            n:
            feature_delimiter:
            **kwargs:
        """

        super().__init__(**kwargs)
        self.feature_delimiter = feature_delimiter
        self.max_classes = max_classes
        self.model: typing.Dict[str, typing.Dict[str, float]] = {}

    def _feature_to_string(self, feature: np.ndarray):
        return reduce(lambda a, b: f'{a}{self.feature_delimiter}{b}', feature)

    def fit(self, X: np.ndarray, y: np.ndarray) -> 'NGram':

        self.model = {}

        X, y = check_X_y(X, y)

        if self.max_classes is None:
            self.max_classes = int(np.argmax(y))

        for feature, label in zip(X, y):
            prefix = self._feature_to_string(feature)
            if prefix not in self.model.keys():
                self.model[prefix] = np.zeros(self.max_classes)
            self.model[prefix][int(label)] += 1

        for prefix in self.model.keys():
            total = np.sum(self.model[prefix])
            if total > 0:
                self.model[prefix] /= total

        return self

    def predict(self, X: np.ndarray) -> np.ndarray:
        predictions = []

        for feature in X:
            prefix = self._feature_to_string(feature)
            if prefix in self.model:
                predictions.append(self.model[prefix])
            else:
                predictions.append(np.zeros(self.max_classes) - 1.0)

        return np.array(predictions)

    def score(self, X: np.ndarray, y: np.ndarray, sample_weight=None) -> float:
        predictions = []

        for feature in X:
            prefix = self._feature_to_string(feature)
            if prefix in self.model:
                predictions.append(self.model[prefix])
            else:
                predictions.append(np.zeros(self.max_classes) - 1.0)

        return ProbabilisticVectorMetric(y, np.array(predictions)).accuracy()
