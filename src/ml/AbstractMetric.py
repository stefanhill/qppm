import abc


class AbstractMetric(abc.ABC):

    def __init__(self, y_true, y_pred):
        self._y_true = y_true
        self._y_pred = y_pred

    @property
    def y_true(self):
        return self._y_true

    @y_true.setter
    def y_true(self, value):
        self._y_true = value

    @property
    def y_pred(self):
        return self._y_pred

    @y_pred.setter
    def y_pred(self, value):
        self._y_pred = value

    @abc.abstractmethod
    def accuracy(self):
        pass

