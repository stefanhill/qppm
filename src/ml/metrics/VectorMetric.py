from ml.AbstractMetric import AbstractMetric


class VectorMetric(AbstractMetric):

    def __init__(self, *args):
        super().__init__(*args)

    def accuracy(self):
        predicted_true = 0
        for label, prediction in zip(self.y_true, self.y_pred):
            if label - prediction == 0:
                predicted_true += 1
        if len(self.y_true):
            return predicted_true / len(self.y_true)
        else:
            return -1.0
