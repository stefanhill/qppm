import copy
import math

import numpy as np
from pennylane import numpy as pnp

from data.handler.EventLogHandler import EventLogHandler
from ml.EventLogEncoder import EventLogEncoder
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder


class MLUtils:

    @staticmethod
    def float_to_binary_array(value, dim):
        """
        Binary label encoding
        Args:
            value:
            dim:

        Returns:

        """
        r = pnp.zeros(dim)
        j_ = 1
        for i_ in range(dim):
            if value % j_ == 0:
                r[i_] = 1
            j_ = j_ * 2
        return r[::-1]

    @staticmethod
    def binary_array_argmax(arr) -> float:
        r = 0
        for i_, e_ in enumerate(reversed(arr)):
            r += abs(round(float(e_))) * 2 ** i_
        return float(r)

    @staticmethod
    def next_2_pot(i):
        """
        Find next higher integer which is a power of two
        Args:
            i:

        Returns:

        """
        k = 1
        while k < i:
            k = k << 1
        return int(math.log2(k))

    @staticmethod
    def min_max_normalization(arr: np.ndarray, minimum: float, maximum: float) -> np.ndarray:
        """

        Args:
            arr: array of input values to be normalized
            minimum: normalization minimum
            maximum: normalization maximum

        Returns:
            normalized array

        """
        return (arr - minimum) / (maximum - minimum)

    @staticmethod
    def autonormalize_encoder(enc: 'EventLogEncoder', *datasets: 'EventLogHandler') -> 'EventLogEncoder':
        """

        Derives minima and maxima to normalize an encoder

        Args:
            enc: encoder without normalization
            *datasets: event log handlers from which to extract data

        Returns:
            Normalized event log encoder

        """
        builder_ = EventLogEncodingBuilder() \
            .add(enc)

        minimum = 0
        maximum = 1

        for dataset in datasets:
            x_, _ = builder_.build(dataset)
            minimum = min(minimum, min(x_))
            maximum = max(maximum, max(x_))

        temp_enc = copy.deepcopy(enc)
        temp_enc._normalization = (float(minimum), float(maximum))
        return temp_enc

    @staticmethod
    def prep_features_for_amplitude_encoding(X: np.ndarray) -> np.ndarray:
        X_norm = np.empty(shape=X.shape)
        for i_, x_ in enumerate(X):
            norm_ = np.sum(abs(x_)**2)
            X_norm[i_] = x_ / np.sqrt(norm_)
        return X_norm

