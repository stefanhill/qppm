import abc
import typing

import numpy as np


class AbstractPrediction(abc.ABC):

    def __init__(self,
                 classes=None):
        self._classes = classes if classes is not None else []

    @abc.abstractmethod
    def get_probs(self) -> np.ndarray:
        pass

    @abc.abstractmethod
    def get_probs_annotated(self) -> typing.Dict[typing.Any, float]:
        pass

    @abc.abstractmethod
    def get_max(self) -> typing.Any:
        pass
