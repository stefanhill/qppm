import typing

import numpy as np

from ml.encoding.FeatureLabelProcessor import FeatureLabelProcessor


class Slice(FeatureLabelProcessor):

    def __init__(self, bounds: typing.Tuple[int, int]):
        self._lower = bounds[0]
        self._upper = bounds[1]

    def process(self, data: typing.Tuple[np.ndarray, np.ndarray]) -> typing.Tuple[np.ndarray, np.ndarray]:
        return data[0][self._lower:self._upper], data[1][self._lower:self._upper]
