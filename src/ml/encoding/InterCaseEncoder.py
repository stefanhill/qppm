import abc

import numpy as np
import pandas as pd

from data.handler.EventLogHandler import EventLogHandler
from ml.EventLogEncoder import EventLogEncoder


class InterCaseEncoder(EventLogEncoder, abc.ABC):
    """
    Abstract class for the inter-case encoders

    Implements a method to create the window for a reference event
    """

    def __init__(self, timeframe=pd.Timedelta(days=1)):
        """
        Initializes the inter-case encoder

        Args:
            timeframe: pandas timedelta for the peer cases window
        """
        self._timeframe = timeframe

    @abc.abstractmethod
    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        pass

    def get_window(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> pd.DataFrame:
        """
        Returns all events inside the specified window for a reference event in the data handler

        Requires the data handler to contain a time series dataframe of all events

        Args:
            data_handler: data handler with event log and time series dataframe
            trace_identifier: reference trace id
            event_identifier: reference event id

        Returns:
            A pandas dataframe with all events in the specified reference window starting at the reference event

        """
        time_series = data_handler.time_series
        trace = data_handler.dataset.get_trace_by_id(trace_identifier)
        reference_event = trace.events[event_identifier]
        return time_series[(time_series.index > reference_event.timestamp - self._timeframe) & (
                time_series.index < reference_event.timestamp)]
