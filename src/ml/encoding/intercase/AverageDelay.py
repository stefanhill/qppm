import datetime
import typing

import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.MLUtils import MLUtils
from ml.encoding.InterCaseEncoder import InterCaseEncoder


class AverageDelay(InterCaseEncoder):
    """
    Inter-case encoder to return the average delay of all peer cases in the reference window
    """

    def __init__(self,
                 to_seconds: bool = True,
                 normalization: typing.Union[typing.Tuple[float, float], str] = None,
                 **kwargs):
        super().__init__(**kwargs)
        self._convert_to_seconds = to_seconds
        self._normalization = normalization

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        """
        Encode the average delay of all peer cases in the reference window

        Args:
            data_handler: data handler with event log, time series, process definition and performance spectrum
            trace_identifier: reference trace id
            event_identifier: reference event id

        Returns:
            A numpy array with one datetime timedelta
            datetime.timedelta(0.0) if there are no events in the reference window

        """
        window = self.get_window(data_handler, trace_identifier, event_identifier)

        total_average_delay = datetime.timedelta(0.0)
        # Iterate over all peer cases in the reference window
        for trace_identifier_, group_ in window.groupby('trace_identifier', as_index=False):
            # Iterate over all pairs of subsequent events in the peer case
            for i_ in range(1, len(group_)):
                # Take the timedelta of the event pair
                dt_current = group_.index[i_] - group_.index[i_ - 1]
                # Get activity id of start and end event
                start_segment = data_handler.process_definition.activity_to_id[group_.iloc[i_ - 1]['concept:name']]
                end_segment = data_handler.process_definition.activity_to_id[group_.iloc[i_]['concept:name']]
                # Take the average duration of the performance spectrum
                dt_average_segment = data_handler.performance_spectrum \
                    .get_segment_by_event_classifier(start_segment, end_segment, create_if_not_exists=True) \
                    .average_segment_duration
                # Add timedelta to the total average delay
                total_average_delay += dt_current - dt_average_segment

        if self._convert_to_seconds:
            if self._normalization is not None:
                return MLUtils.min_max_normalization(np.array([total_average_delay.total_seconds()]),
                                                     *self._normalization)
            else:
                return np.array([total_average_delay.total_seconds()])
        else:
            return np.array([total_average_delay])
