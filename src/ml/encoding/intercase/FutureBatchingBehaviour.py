import datetime

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin

from data.handler.EventLogHandler import EventLogHandler
from ml.AbstractClassifier import AbstractClassifier
from ml.classifier.NGram import NGram
from ml.encoding.BuilderConfiguration import BuilderConfiguration
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.InterCaseEncoder import InterCaseEncoder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder


class FutureBatchingBehaviour(InterCaseEncoder):
    """
    Inter-case encoder to return statistics about batching in predicted next segment
    """

    def __init__(self,
                 segment_classifier=NGram(),
                 segment_encoding_builder=None,
                 include_average_times: bool = False,
                 to_seconds: bool = True,
                 **kwargs):
        """
        Initialises the future batching encoder

        Args:
            segment_classifier: A classifier to predict the future segment
            include_average_times: Bool whether to return also average segment duration and batch duration
            to_seconds: Convert output datetime timedelta to seconds
            **kwargs:
        """
        super().__init__(**kwargs)
        self._segment_classifier: 'NGram' = segment_classifier
        if segment_encoding_builder is None:
            self._segment_encoding_builder = EventLogEncodingBuilder() \
                .add(IndexBasedEncoder(window=4))
        else:
            self._segment_encoding_builder = segment_encoding_builder
        self._include_average_times = include_average_times
        self._convert_to_seconds = to_seconds

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        """
        Encode batching statistics for the future segment

        Args:
            data_handler: data handler with event log, performance spectrum and time series
            trace_identifier: reference trace id
            event_identifier: reference event id

        Returns:
            A numpy array with the batching statistics of the most likely next segment
            If object is initialised with include_average_times, average segment duration and batch duration are added
            as timedelta

        """
        # Check whether batching patterns are available for the segment
        if data_handler.performance_spectrum is None:
            raise ReferenceError('Data Handler does not contain performance spectrum!')

        # TODO: error handling if segment classifier is not trained

        # Create a builder configuration and predict next segment
        builder_configuration = BuilderConfiguration(trace_identifier, event_identifier)
        X, _ = self._segment_encoding_builder.build(data_handler, builder_configuration=builder_configuration,
                                                    predict=True)
        segment_prediction = self._segment_classifier.predict(X)[0]

        current_segment = data_handler.process_definition.activity_to_id[
            data_handler.dataset.get_trace_by_id(trace_identifier).events[event_identifier].attributes['concept:name']]

        next_segment = np.argmax(segment_prediction)

        # Try to load predicted segment and return batching statistics
        try:
            segment = data_handler.performance_spectrum.get_segment_by_event_classifier(current_segment, next_segment)
            if self._include_average_times:
                if self._convert_to_seconds:
                    return np.array([segment.batch_ratio, segment.average_segment_duration.total_seconds(),
                                     segment.average_batch_duration.total_seconds()])
                else:
                    return np.array([segment.batch_ratio, segment.average_segment_duration,
                                     segment.average_batch_duration])
            else:
                return np.array([segment.batch_ratio])

        # If there are no statistics for the predicted segment, return -1
        except ReferenceError:
            if self._include_average_times:
                if self._convert_to_seconds:
                    return np.array([0.0, datetime.timedelta(0.0).total_seconds(),
                                     datetime.timedelta(0.0).total_seconds()])
                else:
                    return np.array([0.0, datetime.timedelta(0.0), datetime.timedelta(0.0)])
            else:
                return np.array([0.0])
