import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.EventLogEncoder import EventLogEncoder


class AggregationEncoder(EventLogEncoder):

    def __init__(self,
                 mode='frequency'):
        self._mode = mode

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        no_distinct_events = len(data_handler.process_definition)
        trace = data_handler.dataset.get_trace_by_id(trace_identifier)
        feature_vector = np.zeros(no_distinct_events)
        for j in range(len(trace.events)):
            if j <= event_identifier:
                event_class = data_handler.process_definition.activity_to_id[trace.events[j].attributes['concept:name']]
                if self._mode == 'frequency':
                    feature_vector[event_class] += 1
                elif self._mode == 'binary':
                    feature_vector[event_class] = 1
            else:
                break
        return feature_vector
