import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.EventLogEncoder import EventLogEncoder


class IndexBasedEncoder(EventLogEncoder):

    def __init__(self,
                 window: int = 5,
                 normalization: bool = False,
                 event_attribute: str = 'concept:name'):
        self._window = window
        self._normalization = normalization
        self._event_attribute = event_attribute

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        """Creates a simple index based encoding for a given trace and given events

        Args:
            data_handler: collection of trace to work on
            trace_identifier: trace id
            event_identifier: event index

        Returns: index based encoding, normalized if IndexBasedEncoder is initialized with normalization=True

        """
        # get the trace
        trace = data_handler.dataset.get_trace_by_id(trace_identifier)
        # create a feature vector of length window
        feature_vector = -1 * np.ones(self._window)
        # set start index to
        lower_index = max(0, event_identifier - self._window)
        # iterate previous indices and append to feature vector
        i = 0
        for j in range(lower_index, event_identifier):
            # add event class to feature vector
            # TODO: perform checks whether event attributes exist (concept:name always should)
            feature_vector[i] = data_handler.process_definition.activity_to_id[
                trace.events[j].attributes[self._event_attribute]]
            i += 1

        # for amplitude normalization divide by the number of distinct events
        if self._normalization:
            # +1 because of nan events==-1 and to avoid division by zero
            return (feature_vector + 1) / (len(data_handler.process_definition) + 1)
        else:
            return feature_vector
