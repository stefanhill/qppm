import datetime
import os
import pickle
import typing

from sklearn.base import TransformerMixin
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.QKE import QKE
from ml.classifier.VQC import VQC
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.InterCaseEncoder import InterCaseEncoder
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from utils.Path import Path


class APIUtils:

    @staticmethod
    def get_classifier_by_name(classifier_name: str, data_handler: 'EventLogHandler', request_data):
        if classifier_name == "XGBoost":
            return XGBClassifier()
        elif classifier_name == "VQC":
            return VQC(n_layers=2, state_preparation=QuantumEmbedding.zz, epochs=20,
                       min_wires=len(data_handler.process_definition), use_jax=False, debug=True)
        elif classifier_name == "QKE":
            kernel_name = request_data['parameters']['Quantum-Kernel'][0]
            return QKE(n_layers=request_data['parameters']['Kernel-Layers'],
                       kernel=getattr(QuantumEmbedding, kernel_name),
                       use_jax=request_data['parameters']['Interface'][0] == 'jax',
                       debug=True)
        else:
            return RandomForestClassifier(max_depth=4)

    @staticmethod
    def parse_intercase_encoder(encoder_name: str, timedelta: 'datetime.timedelta') -> 'InterCaseEncoder':
        if encoder_name == 'NoPeerCases':
            return NoPeerCases(timeframe=timedelta)
        elif encoder_name == 'PeerActivityCount':
            return PeerActivityCount(timeframe=timedelta)
        else:
            raise ReferenceError(f'Encoder {encoder_name} is not a valid parameter for the intercase encoder!')

    @staticmethod
    def load_encoding_pipeline(path: typing.Union[str, Path], pipeline_name: str) \
            -> typing.Tuple['EventLogEncodingBuilder', 'TransformerMixin']:
        if isinstance(path, Path):
            path = str(path)
        builder_path = os.path.join(path, f'{pipeline_name}.builder')
        scaler_path = os.path.join(path, f'{pipeline_name}.scaler')
        return pickle.load(open(builder_path, mode='rb')), pickle.load(open(scaler_path, mode='rb'))

    @staticmethod
    def store_encoding_pipeline(builder: 'EventLogEncodingBuilder', scaler: 'TransformerMixin',
                                path: typing.Union[str, Path], pipeline_name: str):
        if isinstance(path, Path):
            path = str(path)
        builder_path = os.path.join(path, f'{pipeline_name}.builder')
        scaler_path = os.path.join(path, f'{pipeline_name}.scaler')
        pickle.dump(builder, open(builder_path, mode='wb'))
        pickle.dump(scaler, open(scaler_path, mode='wb'))
