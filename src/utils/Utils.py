import functools
import pickle
import sys
import typing

import pm4py
from sklearn.base import ClassifierMixin

from data.processing.AbstractProcessor import AbstractProcessor
from utils.Path import Path


class Utils:

    @staticmethod
    def apply_preprocessing_chain(event_log: 'pm4py.objects.log.obj.EventLog',
                                  preprocessing_chain: typing.List['AbstractProcessor']):
        # TODO make option for deepcopy
        traces = getattr(event_log, '_list')
        for trace in traces:
            for preprocessor in preprocessing_chain:
                trace = preprocessor.process(trace)
        setattr(event_log, '_list', traces)
        return event_log

    @staticmethod
    def list_to_string(input_list: typing.List[str]) -> str:
        return functools.reduce(lambda a, b: a + ',' + b, input_list)

    @staticmethod
    def get_memory_size(obj: object, max_depth=5, size=0) -> int:
        """Recursive function to determine the memory size of an object

        Args:
            obj: object to be measured
            max_depth: how many recursion steps to perform before break
            size: current size of the object

        Returns:
            size of the object

        """
        if max_depth != 0:
            try:
                for attribute_name in obj.__dict__.keys():
                    size += Utils.get_memory_size(getattr(obj, attribute_name), max_depth=max_depth - 1, size=size)
            except AttributeError:
                pass
            size += sys.getsizeof(obj)
        return size

    @staticmethod
    def load_classifier(path: typing.Union[str, Path]) -> 'ClassifierMixin':
        if isinstance(path, Path):
            path = str(path)
        return pickle.load(open(path, mode='rb'))

    @staticmethod
    def save_classifier(clf: 'ClassifierMixin', path: typing.Union[str, Path]):
        if isinstance(path, Path):
            path = str(path)
        pickle.dump(clf, open(path, mode='wb'))

    @staticmethod
    def merge_ngram_probabilites(prediction: typing.Dict[str, typing.Dict[str, typing.Dict[str, float]]]):
        return_probabilities = {}
        for case_id, grams in prediction.items():
            return_probabilities[case_id] = {}
            n = len(grams.keys())
            for _, probs in grams.items():
                for event_name, prob in probs.items():
                    if event_name not in return_probabilities[case_id].keys():
                        return_probabilities[case_id][event_name] = []
                    return_probabilities[case_id][event_name].append(prob / n)
            return_probabilities[case_id] = {
                event_name: functools.reduce(lambda a, b: a + b, event_probabilities) for
                event_name, event_probabilities in return_probabilities[case_id].items()
            }
        return return_probabilities
