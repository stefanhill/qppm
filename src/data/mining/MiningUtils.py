import pandas as pd
from deprecation import deprecated

from data.dataset.EventLog import EventLog
from data.process.Observation import Observation
from data.process.PerformanceSpectrum import PerformanceSpectrum
from data.process.ProcessDefinition import ProcessDefinition


@deprecated
class MiningUtils:

    @staticmethod
    def process_definition_from_event_log(data: 'EventLog') -> 'ProcessDefinition':
        # TODO: what happens if event names are remapped later?
        activity_to_id = {}
        resource_to_id = {}
        event_id_, resource_id_ = 0, 0
        for trace in data.traces:
            for event in trace.events:
                if event.attributes['concept:name'] not in activity_to_id.keys():
                    activity_to_id[event.attributes['concept:name']] = event_id_
                    event_id_ = event_id_ + 1
                if event.attributes['org:resource'] not in resource_to_id.keys():
                    resource_to_id[event.attributes['org:resource']] = resource_id_
                    resource_id_ = resource_id_ + 1
        return ProcessDefinition(activity_to_id=activity_to_id, resource_to_id=resource_to_id)

    @staticmethod
    def performance_spectrum_from_event_log(data: 'EventLog') -> 'PerformanceSpectrum':
        ps = PerformanceSpectrum()
        for trace in data.traces:
            for i in range(1, len(trace)):
                start_observation = trace.events[i - 1]
                end_observation = trace.events[i]
                start_segment = start_observation.identifier
                end_segment = end_observation.identifier
                segment = ps.get_segment_by_event_classifier(start_segment, end_segment, create_if_not_exists=True)
                segment.observations.append(Observation(start_observation, end_observation))
        return ps

    @staticmethod
    def time_series_from_event_log(data: 'EventLog') -> pd.DataFrame:
        df = {
            'trace_identifier': [],
            'event_identifier': [],
            'concept:name': [],
            'org:resource': []
        }
        timestamp = []
        for trace in data.traces:
            for i in range(len(trace.events)):
                df['trace_identifier'].append(trace.identifier)
                df['event_identifier'].append(i)
                df['concept:name'].append(trace.events[i].attributes['concept:name'])
                df['org:resource'].append(trace.events[i].attributes['org:resource'])
                timestamp.append(trace.events[i].timestamp)
        return pd.DataFrame(df, index=timestamp)
