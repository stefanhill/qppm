import copy
import typing

import numpy as np

from data.AbstractDataInstance import AbstractDataInstance
from data.dataset.Event import Event


class Trace(AbstractDataInstance):

    def __init__(self,
                 identifier: typing.Union[str, int] = np.NaN,
                 attributes: typing.Dict[str, typing.Any] = None):
        super().__init__()

        self._identifier = identifier
        self._attributes = attributes if attributes is not None else {}

    @property
    def events(self) -> typing.List['Event']:
        return self._points

    @events.setter
    def events(self, value: typing.List['Event']):
        self._points = value

    @property
    def identifier(self):
        return self._identifier

    @identifier.setter
    def identifier(self, value):
        self._identifier = value

    @property
    def attributes(self):
        return self._attributes

    @attributes.setter
    def attributes(self, value):
        self._attributes = value

    def __getitem__(self, item):
        return_trace = Trace(attributes=copy.deepcopy(self._attributes))
        return_events = []
        if isinstance(item, int):
            return_events.append(copy.deepcopy(self.events[item]))
        elif isinstance(item, slice):
            return_events = copy.deepcopy(self.events[item])
        elif isinstance(item, list):
            for elem in item:
                return_events.append(copy.deepcopy(self.events[elem]))
        return_trace.events = return_events
        return return_trace
