import typing

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogFilter import AbstractEventLogFilter


class IndexFilter(AbstractEventLogFilter):

    def __init__(self, indices=None, **kwargs):
        super().__init__(**kwargs)
        self._indices = indices if indices is not None else []

    def process(self, data: 'EventLog') -> typing.Tuple['EventLog', 'EventLog']:
        # TODO: static attributes from EventLog
        return_data = EventLog()
        if not self._return_complement:
            traces = []
            for index in self._indices:
                traces.append(data.traces[index])
            return_data.traces = traces
            return return_data, EventLog()
