import random
import time
import typing

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogFilter import AbstractEventLogFilter
from data.processing.filter.IndexFilter import IndexFilter


class RandomTraces(AbstractEventLogFilter):

    def __init__(self, no_traces: int = 1, min_trace_length: int = 4, seed: int = None, **kwargs):
        super().__init__(**kwargs)
        self._no_traces = no_traces
        self._min_trace_length = min_trace_length
        self._seed = seed if seed is not None else time.clock()

        self._max_iterations = self._no_traces * 100

    def process(self, data: 'EventLog') -> typing.Tuple['EventLog', 'EventLog']:
        random.seed(self._seed)
        indices = []
        for _ in range(self._max_iterations):
            if len(indices) >= self._no_traces:
                break
            random_index = random.choice(range(0, len(data)))
            if len(data.traces[random_index]) >= self._min_trace_length and random_index not in indices:
                indices.append(random_index)
        return IndexFilter(indices=indices, return_complement=self._return_complement).process(data)
