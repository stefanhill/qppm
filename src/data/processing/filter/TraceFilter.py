import typing

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogFilter import AbstractEventLogFilter
from data.processing.filter.IndexFilter import IndexFilter


class TraceFilter(AbstractEventLogFilter):

    def __init__(self, attribute_name='concept:name', filter_values=None, **kwargs):
        super().__init__(**kwargs)
        self._attribute_name = attribute_name
        self._filter_values = filter_values if filter_values is not None else []

    def process(self, data: 'EventLog') -> typing.Tuple['EventLog', 'EventLog']:
        indices = []
        for i, trace in enumerate(data.traces):
            if self._attribute_name in trace.attributes.keys() \
                    and trace.attributes[self._attribute_name] in self._filter_values:
                indices.append(i)
        return IndexFilter(indices=indices, return_complement=self._return_complement).process(data)
