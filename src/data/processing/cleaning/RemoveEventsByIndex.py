import typing

from data.dataset.Trace import Trace
from data.processing.AbstractTraceProcessor import AbstractTraceProcessor


class RemoveEventsByIndex(AbstractTraceProcessor):

    def __init__(self, indices: typing.List[int] = None):
        self._indices = [] if indices is None else indices

    def process(self, trace: 'Trace') -> 'Trace':
        for i in sorted(self._indices, reverse=True):
            trace.events.pop(i)
        return trace
