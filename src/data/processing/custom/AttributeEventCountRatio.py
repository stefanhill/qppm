from data.dataset import EventLog
from data.dataset.EndEvent import EndEvent
from data.dataset.StartEvent import StartEvent
from data.processing.AbstractEventLogProcessor import AbstractEventLogProcessor


class AttributeEventCountRatio(AbstractEventLogProcessor):

    def __init__(self,
                 attribute_name: str = 'org:resource',
                 to_trace_attribute: str = 'RESOURCE_EVENT_RATIO',
                 exclude_generic_events: bool = True,
                 default_value: float = 0.0):
        self._attribute_name = attribute_name
        self._to_trace_attribute = to_trace_attribute
        self._exclude_generic_events = exclude_generic_events
        self._default_value = default_value

    def process(self, data: 'EventLog') -> 'EventLog':
        for trace in data.traces:
            distinct_attributes = set()
            no_events = 0
            for event in trace.events:
                if not self._exclude_generic_events or \
                        not (isinstance(event, StartEvent) or isinstance(event, EndEvent)):
                    no_events = no_events + 1
                    if self._attribute_name in event.attributes.keys():
                        distinct_attributes.add(event.attributes[self._attribute_name])
            if no_events > 0:
                trace.attributes[self._to_trace_attribute] = len(distinct_attributes) / no_events
            else:
                trace.attributes[self._to_trace_attribute] = self._default_value
        return data
