import abc
import typing

from data.processing.AbstractProcessor import AbstractProcessor


class AbstractFilter(AbstractProcessor, abc.ABC):

    def __init__(self, return_complement=False):
        self._return_complement = return_complement

    @abc.abstractmethod
    def process(self, data: typing.Any) -> typing.Tuple[typing.Any, typing.Any]:
        pass
