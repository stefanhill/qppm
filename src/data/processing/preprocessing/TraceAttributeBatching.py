import typing

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogProcessor import AbstractEventLogProcessor


class TraceAttributeBatching(AbstractEventLogProcessor):

    def __init__(self,
                 attribute_name: str = 'concept:name',
                 batches: typing.List[typing.Tuple[float, float, str]] = None,
                 empty_batch: str = 'NO_BATCH'):
        self._attribute_name = attribute_name
        self._batches = batches
        self._empty_batch = empty_batch

    def process(self, data: 'EventLog') -> 'EventLog':
        for trace in data.traces:
            if self._attribute_name in trace.attributes.keys():
                batch_set = False
                for batch in self._batches:
                    if batch[0] <= float(trace.attributes[self._attribute_name]) < batch[1]:
                        trace.attributes[self._attribute_name] = batch[2]
                        batch_set = True
                        break
                if not batch_set:
                    trace.attributes[self._attribute_name] = self._empty_batch
        return data
