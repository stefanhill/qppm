from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogProcessor import AbstractEventLogProcessor


class TraceCut(AbstractEventLogProcessor):

    def __init__(self, cut_position: int = 4, remove_end_events: bool = True):
        self._cut_position = cut_position
        self._remove_end_events = remove_end_events

    def process(self, data: 'EventLog') -> 'EventLog':
        for trace in data.traces:
            if self._remove_end_events:
                trace.events.pop()
            i = len(trace)
            while i > self._cut_position:
                trace.events.pop()
                i = i - 1
        return data
