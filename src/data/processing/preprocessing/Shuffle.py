import random

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogProcessor import AbstractEventLogProcessor


class Shuffle(AbstractEventLogProcessor):

    def process(self, data: 'EventLog') -> 'EventLog':
        indices = list(range(len(data)))
        random.shuffle(indices)
        return data[indices]
