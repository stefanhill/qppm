import typing

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor


class RenameEvents(AbstractProcessor):

    def __init__(self, name_mapping: typing.Dict[str, str]):
        self._name_mapping = name_mapping

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        temp_list = getattr(trace, '_list')
        for event in temp_list:
            event_properties = getattr(event, '_dict')
            if event_properties['concept:name'] in self._name_mapping.keys():
                event_properties['concept:name'] = self._name_mapping[event_properties['concept:name']]
            setattr(event, '_dict', event_properties)
        setattr(trace, '_list', temp_list)
        return trace
