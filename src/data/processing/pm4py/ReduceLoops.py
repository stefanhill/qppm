import typing

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor
from data.processing.pm4py.RemoveEventsByIndex import RemoveEventsByIndex


class ReduceLoops(AbstractProcessor):

    def __init__(self):
        pass

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        event_names: typing.List[str] = list(
            map(lambda e: getattr(e, '_dict')['concept:name'], getattr(trace, '_list')))
        remove_indices = []
        c = 0
        prev_event = ''
        for i, event in enumerate(event_names):
            if prev_event == event:
                c += 1
                if c >= 2:
                    remove_indices.append(i)
            else:
                c = 0
            prev_event = event
        return RemoveEventsByIndex(indices=remove_indices).process(trace)
