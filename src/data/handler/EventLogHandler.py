import copy
import datetime
import typing

import pandas as pd

from data.AbstractDataHandler import AbstractDataHandler
from data.dataset.EventLog import EventLog
from data.loader.log.XESDataLoader import XESDataLoader
from data.process.Observation import Observation
from data.process.PerformanceSpectrum import PerformanceSpectrum
from data.process.ProcessDefinition import ProcessDefinition
from data.processing.AbstractFilter import AbstractFilter
from utils.Decorators import timer, memory
from utils.Path import Path


class EventLogHandler(AbstractDataHandler):

    def __init__(self):
        super().__init__()
        self._process_definition = None
        self._performance_spectrum = None
        self._time_series = None

    @property
    def dataset(self) -> 'EventLog':
        return self._dataset

    @dataset.setter
    def dataset(self, value: 'EventLog'):
        self._dataset = value

    @property
    def process_definition(self) -> 'ProcessDefinition':
        return self._process_definition

    @process_definition.setter
    def process_definition(self, value: 'ProcessDefinition'):
        self._process_definition = value

    @property
    def performance_spectrum(self) -> 'PerformanceSpectrum':
        return self._performance_spectrum

    @performance_spectrum.setter
    def performance_spectrum(self, value: 'PerformanceSpectrum'):
        self._performance_spectrum = value

    @property
    def time_series(self):
        return self._time_series

    @time_series.setter
    def time_series(self, value):
        self._time_series = value

    @timer
    @memory
    def get(self, item: typing.Union[int, typing.List[int], slice, 'AbstractFilter']) -> 'EventLogHandler':
        # TODO make copy of static elements
        if isinstance(item, int) or isinstance(item, list) or isinstance(item, slice):
            return copy.deepcopy(self[item])
        elif isinstance(item, AbstractFilter):
            return_handler = EventLogHandler()
            return_handler.dataset, _ = item.process(self.dataset)
            return copy.deepcopy(return_handler)
        else:
            return EventLogHandler()

    @timer
    @memory
    def load(self, path: typing.Union[str, 'Path'], loader_kwargs: typing.Dict[str, typing.Any] = None):
        loader_kwargs = loader_kwargs if loader_kwargs is not None else {}
        loader = XESDataLoader(**loader_kwargs)
        self.dataset = loader.load(path)

        self.mine_process_definition()
        self.mine_time_series()
        self.mine_performance_spectrum()

    def mine_process_definition(self):
        # TODO: what happens if event names are remapped later? -> rerun method in that case
        # TODO: make this function save and assure resources and activities to be set
        activity_to_id = {}
        resource_to_id = {}
        event_id_, resource_id_ = 0, 0
        for trace in self.dataset.traces:
            for event in trace.events:
                if 'concept:name' in event.attributes.keys() \
                        and event.attributes['concept:name'] not in activity_to_id.keys():
                    activity_to_id[event.attributes['concept:name']] = event_id_
                    event_id_ = event_id_ + 1
                if 'org:resource' in event.attributes.keys() \
                        and event.attributes['org:resource'] not in resource_to_id.keys():
                    resource_to_id[event.attributes['org:resource']] = resource_id_
                    resource_id_ = resource_id_ + 1
        self.process_definition = ProcessDefinition(activity_to_id=activity_to_id, resource_to_id=resource_to_id)

    def mine_time_series(self):
        # TODO: make this function robust against missing values
        df = {
            'trace_identifier': [],
            'event_identifier': [],
            'concept:name': [],
            'org:resource': []
        }
        timestamp = []
        for trace in self.dataset.traces:
            for i in range(len(trace.events)):
                df['trace_identifier'].append(trace.identifier)
                df['event_identifier'].append(i)
                if 'concept:name' in trace.events[i].attributes.keys():
                    df['concept:name'].append(trace.events[i].attributes['concept:name'])
                else:
                    df['concept:name'].append('UNDEFINED')
                if 'org:resource' in trace.events[i].attributes.keys():
                    df['org:resource'].append(trace.events[i].attributes['org:resource'])
                else:
                    df['org:resource'].append('UNDEFINED')
                timestamp.append(trace.events[i].timestamp)
        self.time_series = pd.DataFrame(df, index=timestamp)

    def mine_performance_spectrum(self):
        ps = PerformanceSpectrum()
        for trace in self.dataset.traces:
            for i in range(1, len(trace)):
                start_observation = trace.events[i - 1]
                end_observation = trace.events[i]
                start_segment = self.process_definition.activity_to_id[start_observation.attributes['concept:name']]
                end_segment = self.process_definition.activity_to_id[end_observation.attributes['concept:name']]
                segment = ps.get_segment_by_event_classifier(start_segment, end_segment, create_if_not_exists=True)
                segment.observations.append(Observation(start_observation, end_observation))
        self.performance_spectrum = ps

    def mine_batching_patterns(self, max_delay=datetime.timedelta(hours=12), min_batch_size=10):
        for segment in self.performance_spectrum.segments:
            segment.observations = sorted(segment.observations, key=lambda x: x.start.timestamp)
            segment.observations = sorted(segment.observations, key=lambda x: x.end.timestamp)
            batches = []
            temp_batch = []
            for i in range(1, len(segment.observations)):
                prev_obs = segment.observations[i - 1]
                current_obs = segment.observations[i]
                temp_batch.append(prev_obs)
                if prev_obs.end.timestamp <= current_obs.end.timestamp <= max_delay + prev_obs.end.timestamp \
                        and current_obs.start.timestamp >= prev_obs.start.timestamp:
                    continue
                else:
                    if len(temp_batch) >= min_batch_size:
                        batches.append(copy.deepcopy(temp_batch))
                    temp_batch = []
            segment.batches = batches
