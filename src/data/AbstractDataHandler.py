import abc
import copy
import typing

from data.AbstractDataset import AbstractDataset
from data.processing.AbstractFilter import AbstractFilter
from data.processing.AbstractProcessor import AbstractProcessor
from utils.Path import Path


class AbstractDataHandler(abc.ABC):

    def __init__(self):
        self._dataset = None
        self._hidden_dataset = None

    @property
    def dataset(self) -> 'AbstractDataset':
        return self._dataset

    @dataset.setter
    def dataset(self, value: 'AbstractDataset'):
        self._dataset = value

    @abc.abstractmethod
    def load(self, path: typing.Union[str, 'Path']):
        pass

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, item) -> 'AbstractDataHandler':
        data_handler = copy.deepcopy(self)
        data_handler.dataset = data_handler.dataset[item]
        return data_handler

    def get(self, item: typing.Union[int, typing.List[int], slice, 'AbstractFilter']) -> 'AbstractDataHandler':
        # TODO implement on abstract level
        pass

    def process(self, processors: typing.List['AbstractProcessor']):
        for processor in processors:
            self.dataset = processor.process(self.dataset)

    def filter(self, filters: typing.List['AbstractFilter']):
        for filter_ in filters:
            self.dataset, hidden_dataset_ = filter_.process(self.dataset)
            # TODO: merge hidden datasets

    def reset_filter(self):
        # TODO: merge hidden datasets with self.dataset
        pass
