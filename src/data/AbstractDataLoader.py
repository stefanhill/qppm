import abc
import typing

from utils.Path import Path


class AbstractDataLoader(abc.ABC):

    def __init__(self):
        pass

    @abc.abstractmethod
    def load(self, path: typing.Union[str, 'Path']):
        pass

    @abc.abstractmethod
    def save(self, path: typing.Union[str, 'Path']):
        pass
