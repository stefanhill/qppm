import typing


class ProcessDefinition:
    # TODO: create a way to load/save ProcessDefinition

    def __init__(self, activity_to_id: typing.Dict[str, int] = None,
                 resource_to_id: typing.Dict[str, int] = None):
        self._activity_to_id = activity_to_id if activity_to_id is not None else {}
        self._resource_to_id = resource_to_id if resource_to_id is not None else {}
        self._model = None

    @property
    def activity_to_id(self) -> typing.Dict[str, int]:
        return self._activity_to_id

    @activity_to_id.setter
    def activity_to_id(self, value: typing.Dict[str, int]):
        self._activity_to_id = value

    @property
    def resource_to_id(self) -> typing.Dict[str, int]:
        return self._resource_to_id

    @resource_to_id.setter
    def resource_to_id(self, value: typing.Dict[str, int]):
        self._resource_to_id = value

    def __len__(self):
        """Length is defined as the number of activities in the process model

        Returns: length as integer

        """
        return len(self.activity_to_id.keys())
