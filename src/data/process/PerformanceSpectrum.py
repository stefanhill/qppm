import typing

from data.process.Segment import Segment


class PerformanceSpectrum:

    def __init__(self):
        self._segments = []

    @property
    def segments(self) -> typing.List['Segment']:
        return self._segments

    @segments.setter
    def segments(self, value: typing.List['Segment']):
        self._segments = value

    def get_segment_by_event_classifier(self, start_event, end_event, create_if_not_exists=False):
        for segment in self.segments:
            if segment.start == start_event and segment.end == end_event:
                return segment
        if create_if_not_exists:
            segment = Segment(start_event, end_event)
            self.segments.append(segment)
            return segment
        else:
            raise ReferenceError(
                'Segment does not exist! Try another event classifier or run with create_if_not_exists=True')
