import typing

import pm4py

from data.AbstractDataLoader import AbstractDataLoader
from data.converter.PM4PYConverter import PM4PYConverter
from data.dataset.EventLog import EventLog
from utils.Path import Path


class XESDataLoader(AbstractDataLoader):

    def __init__(self, wrap_trace: bool = False):
        super().__init__()
        self._wrap_trace = wrap_trace

    def load(self, path: typing.Union[str, 'Path']) -> 'EventLog':
        pm4py_event_log = pm4py.read_xes(path)
        return PM4PYConverter(pm4py_event_log, wrap_trace=self._wrap_trace).to_event_log()

    def save(self, path: typing.Union[str, 'Path']):
        """

        TODO: implement

        Args:
            path:

        Returns:

        """
        pass
