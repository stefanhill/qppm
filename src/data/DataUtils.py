from datetime import datetime

from data.handler.EventLogHandler import EventLogHandler


class DataUtils:

    @staticmethod
    def determine_average_case_length(data_handler: 'EventLogHandler') -> 'datetime.timedelta':
        timedeltas = []
        for trace in data_handler.dataset.traces:
            if len(trace) > 0:
                timedeltas.append(trace.events[-1].timestamp - trace.events[0].timestamp)
        if len(timedeltas) > 0:
            return sum(timedeltas, datetime.timedelta()) / len(timedeltas)
        else:
            return datetime.timedelta(0)

    @staticmethod
    def determine_median_case_length(data_handler: 'EventLogHandler') -> 'datetime.timedelta':
        timedeltas = []
        for trace in data_handler.dataset.traces:
            if len(trace) > 0:
                timedeltas.append(trace.events[-1].timestamp - trace.events[0].timestamp)
        if len(timedeltas) > 0:
            timedeltas.sort()
            mid = len(timedeltas) // 2
            return (timedeltas[mid] + timedeltas[~mid]) / 2
        else:
            return datetime.timedelta(0)