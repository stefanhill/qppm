import typing
from typing import TYPE_CHECKING

import pennylane as qml
from pennylane import numpy as np
from sklearn.svm import SVC

from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from ml.metrics.VectorMetric import VectorMetric
from utils import DeviceConfiguration
from utils.Decorators import timer, memory
from utils.Enums import PredictionType

if TYPE_CHECKING:
    from ml.encoding.BuilderConfiguration import BuilderConfiguration


class QKE(AbstractClassifier):
    _n_wires = 4
    _dev = qml.device("default.qubit", wires=_n_wires)

    def __init__(self, kernel=DeviceConfiguration.KERNEL, n_layers: int = 2,
                 n_wires: int = DeviceConfiguration.N_QUBITS, **kwargs):

        from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
        from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder

        super().__init__(**kwargs)
        self.model: typing.Dict[str, typing.Dict[str, float]] = {}

        self._kernel_function = kernel
        self._n_layers = n_layers
        self._n_wires = n_wires
        self._x_train = np.array([])

        # Create the zero projector
        self._projector = np.zeros((2 ** self._n_wires, 2 ** self._n_wires))
        self._projector[0, 0] = 1

        self.prediction_type = PredictionType.ACTIVITY
        if self.encoder is None:
            self.encoder = EventLogEncodingBuilder() \
                .add(IndexBasedEncoder(window=self._n_wires, normalization=True))

    def _kernel_matrix(self, A, B):
        """Compute the matrix whose entries are the kernel
           evaluated on pairwise data from sets A and B."""

        @qml.qnode(DeviceConfiguration.DEV)
        def _kernel_embedding(x, y):
            if self._projector is None:
                raise AssertionError('Projector matrix not set!')
            self._kernel_function(x, n_layers=self._n_layers, n_wires=self._n_wires)
            adj_ansatz = qml.adjoint(self._kernel_function)
            adj_ansatz(y, n_layers=self._n_layers, n_wires=self._n_wires)
            return qml.expval(qml.Hermitian(self._projector, wires=range(self._n_wires)))

        return np.array([[_kernel_embedding(a, b) for b in B] for a in A])

    @timer
    @memory
    def fit(self, train_handler: 'EventLogHandler', val_handler: 'EventLogHandler' = None):
        x_train, y_train = self.encoder.build(train_handler)

        x_train = x_train[:40]
        y_train = y_train[:40]
        self._x_train = x_train
        matrix_train = self._kernel_matrix(x_train, x_train)

        self.model = SVC(kernel='precomputed')
        self.model.fit(matrix_train, y_train)

    @timer
    @memory
    def predict(self, predict_handler: 'EventLogHandler',
                builder_configuration: 'BuilderConfiguration' = None) -> 'AbstractPrediction':
        x_predict, _ = self.encoder.build(predict_handler, builder_configuration=builder_configuration, predict=True)

        x_predict = x_predict[40:50]
        matrix_predict = self._kernel_matrix(x_predict, self._x_train)
        prediction = self.model.predict(matrix_predict)

        return prediction

    def evaluate(self, val_handler: 'EventLogHandler') -> 'AbstractMetric':
        x_val, y_val = self.encoder.build(val_handler)

        matrix_val = self._kernel_matrix(x_val, self._x_train)
        prediction = self.model.predict(matrix_val)

        return VectorMetric(x_val, prediction)
