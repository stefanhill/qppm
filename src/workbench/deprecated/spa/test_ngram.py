import logging

from data.dataset.EventLog import EventLog
from data.handler.EventLogHandler import EventLogHandler
from utils.Path import Path
from utils.Utils import Utils
from workbench.deprecated.NGramContext import NGramContext
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
# @Ali: der Pfad kann entweder wie vorher als string oder mit dem neuen Path objekt übergeben werden
# ~ wegen kompatibilität mit PC Mac ;)
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes']))
    # str(Path(['resources', '20220105_test_tokens.xes']))
)

# @Ali: alle features und targets für den encoder
# ~ sind zwar aktuell die gleichen rückgabewerte, aber so sind wir flexibler, falls wir manche features ausschließen möchten
possible_event_features, possible_trace_features = data_handler.get_possible_features()
possible_event_targets, possible_trace_targets = data_handler.get_possible_targets()

# n-gram mit 1 < n < 3 trainieren
encoder = NGramEncoder(event_features=['concept:name'],  # event_features aus dem Frontend von possible_event_features
                       trace_features=['AMOUNT_REQ'],  # trace_features aus dem Frontend von possible_trace_features
                       target='concept:name')  # target aus dem Frontend von possible_event_targets XOR possible_trace_targets

ngram = NGramContext(n=3, encoder=encoder)
ngram.fit(data_handler)

# manuell ein paar fake running instances erstellen
running_instance = EventLogHandler()
running_instance.dataset = EventLog()
running_instance.dataset.instances = data_handler.dataset.instances[0:3]
running_instance.dataset.instances[0].events = running_instance.dataset.instances[0].events[0:3]
running_instance.dataset.instances[1].events = running_instance.dataset.instances[1].events[0:8]
running_instance.dataset.instances[2].events = running_instance.dataset.instances[2].events[0:4]

# @Ali: interface für ein dataset für die prediction
case_id_frontend = 12345
# ~ irgendwann innerhalb der nächsten vier Tage
# -> running_instance = data_handler.filter([CaseIdFilter(case_ids=[case_id_frontend])])
# running_instance kann im Frontend weiter verwendet werden für ngram.predict()

# objekt mit ergebnissen
"""
    case-id1: {
        1-gram: {
            event1: prob1,
            event2: prob2
            ...
        },
        ...,
        n-gram: {
            event1: prob1,
            ...
        }
    }
    case-id2: ...
"""
prediction = ngram.predict(running_instance)
mp = Utils.merge_ngram_probabilites(prediction)
