import pm4py
from workbench.spa_scripts.preprocessing_analyse import file_path

from data.processing.filter.Filters import Filters
from data.processing.pm4py.RemoveEventsByLifecycle import RemoveEventsByLifecycle
from data.processing.pm4py.RenameEvents import RenameEvents
from utils.Utils import Utils

event_log = pm4py.read_xes(file_path('BPI_Challenge_2012.xes'))

name_mapping = {
    'W_Afhandelen leads': 'W_Fixing incoming lead',
    'W_Completeren aanvraag': 'W_Filling in information for the application',
    'W_Valideren aanvraag': 'W_Assessing the application',
    'W_Nabellen offertes': 'W_Calling after sent offers',
    'W_Nabellen incomplete dossiers': 'W_Calling to add missing information to the application'
}

event_log = pm4py.filter_log(Filters.remove_no_user, event_log)

event_log = Utils.apply_preprocessing_chain(event_log, [
    RemoveEventsByLifecycle(lifecycles=['COMPLETE'], is_manual=True),
    RenameEvents(name_mapping=name_mapping),
])

from pm4py.algo.organizational_mining.sna import algorithm as sna
sa_metric = sna.apply(event_log, variant=sna.Variants.JOINTACTIVITIES_LOG)

from pm4py.algo.organizational_mining.sna import util
clustering = util.cluster_affinity_propagation(sa_metric)

event_log = Utils.apply_preprocessing_chain(event_log, [
    RemoveEventsByLifecycle(lifecycle='COMPLETE', is_manual=True),
    RenameEvents(name_mapping=name_mapping),
])

event_log = pm4py.filter_log(Filters.end_events, event_log)

#pm4py.write_xes(event_log, 'C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012_prep_20211203-1000.xes')


print(clustering)
