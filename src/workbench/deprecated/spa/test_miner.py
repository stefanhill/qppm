import pm4py

from data.processing.filter.Filters import Filters
from data.processing.pm4py.ManualRemoveEvents import ManualRemoveEvents
from data.processing.pm4py.RemoveEventsByLifecycle import RemoveEventsByLifecycle
from data.processing.pm4py.RemoveEventsByName import RemoveEventsByName
from data.processing.pm4py.RenameEvents import RenameEvents
from utils.Utils import Utils

event_log = pm4py.read_xes('C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012.xes')

name_mapping = {
    'W_Afhandelen leads': 'W_Fixing incoming lead',
    'W_Completeren aanvraag': 'W_Filling in information for the application',
    'W_Valideren aanvraag': 'W_Assessing the application',
    'W_Nabellen offertes': 'W_Calling after sent offers',
    'W_Nabellen incomplete dossiers': 'W_Calling to add missing information to the application'
}

redundant_events = ['A_PARTLYSUBMITTED', 'O_SELECTED', 'O_CREATED', 'O_ACCEPTED', 'A_REGISTERED', 'A_ACTIVATED',
                    'O_DECLINED']  # @Ali: add other ones if necessary

event_log = Utils.apply_preprocessing_chain(event_log, [
    RemoveEventsByLifecycle(lifecycles=['COMPLETE'], is_manual=True),
    RemoveEventsByName(names=redundant_events),
    ManualRemoveEvents(),
    RenameEvents(name_mapping=name_mapping),
])

# this one removes all cases do end with a W event after preprocessing i.e. unfinished cases
event_log = pm4py.filter_log(Filters.end_events, event_log)

# @Ali: use this to save the log
#pm4py.write_xes(event_log, 'C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012_prep_20211208-1820.xes')

net, im, fm = pm4py.discover_petri_net_alpha(event_log)
from pm4py.objects.conversion.wf_net import converter as wf_net_converter
tree = wf_net_converter.apply(net, im, fm)
from pm4py.objects.conversion.process_tree import converter
bpmn_graph = converter.apply(tree, variant=converter.Variants.TO_BPMN)
pm4py.write_bpmn(bpmn_graph, "export_test.bpmn", enable_layout=True)