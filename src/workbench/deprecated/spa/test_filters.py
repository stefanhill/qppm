import logging

from data.handler.EventLogHandler import EventLogHandler
from data.processing.custom.AttributeEventCountRatio import AttributeEventCountRatio
from data.processing.filter.TraceFilter import TraceFilter
from data.processing.preprocessing.TraceAttributeBatching import TraceAttributeBatching
from data.processing.preprocessing.TraceCut import TraceCut
from utils.Path import Path
from workbench.deprecated.NGramContext import NGramContext
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
# @Ali: der Pfad kann entweder wie vorher als string oder mit dem neuen Path objekt übergeben werden
# ~ wegen kompatibilität mit PC Mac ;)
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
amount_req_batches = [(0, 20000, 'LOW'),
                      (20000, 40000, 'MEDIUM'),
                      (40000, 50000, 'HIGH'),
                      (50000, 1000000, 'SUPER_HIGH')]
peep_ratio_batches = [(0, 0.5, 'FEW_P'),
                      (0.5, 1, 'MANY_P')]
data_handler.process([TraceAttributeBatching(attribute_name='AMOUNT_REQ', batches=amount_req_batches),
                      AttributeEventCountRatio(attribute_name='org:resource', to_trace_attribute='PEEP_RATIO'),
                      TraceAttributeBatching(attribute_name='PEEP_RATIO', batches=peep_ratio_batches)])

# @Ali: alle features und targets für den encoder
# ~ sind zwar aktuell die gleichen rückgabewerte, aber so sind wir flexibler, falls wir manche features ausschließen möchten
possible_event_features, possible_trace_features = data_handler.get_possible_features()
possible_event_targets, possible_trace_targets = data_handler.get_possible_targets()

# n-gram mit 1 < n < 3 trainieren
encoder = NGramEncoder(event_features=['concept:name'],  # event_features aus dem Frontend von possible_event_features
                       trace_features=['AMOUNT_REQ', 'PEEP_RATIO'],  # trace_features aus dem Frontend von possible_trace_features
                       target='concept:name')  # target aus dem Frontend von possible_event_targets XOR possible_trace_targets

ngram = NGramContext(n=3, encoder=encoder)
ngram.fit(data_handler)

# @Ali: interface für ein dataset für die prediction
case_id_frontend = '174084'
running_instance = data_handler.get(TraceFilter(attribute_name='concept:name', filter_values=[case_id_frontend]))
running_instance.process([TraceCut(cut_position=6),
                          AttributeEventCountRatio(attribute_name='org:resource', to_trace_attribute='PEEP_RATIO'),
                          TraceAttributeBatching(attribute_name='PEEP_RATIO', batches=peep_ratio_batches)])
#running_instance = data_handler.get(RandomTraces(no_traces=1, min_trace_length=10))
#running_instance.process([RandomTraceCut(min_trace_length=5)])

last_event = running_instance.dataset.instances[0].events.pop()


# objekt mit ergebnissen
"""
    case-id1: {
        1-gram: {
            event1: prob1,
            event2: prob2
            ...
        },
        ...,
        n-gram: {
            event1: prob1,
            ...
        }
    }
    case-id2: ...
"""
prediction = ngram.predict(running_instance)
