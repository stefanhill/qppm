import logging
import random

from data.handler.EventLogHandler import EventLogHandler
from data.processing.custom.AttributeEventCountRatio import AttributeEventCountRatio
from data.processing.preprocessing.RandomTraceCut import RandomTraceCut
from data.processing.preprocessing.Shuffle import Shuffle
from data.processing.preprocessing.TraceAttributeBatching import TraceAttributeBatching
from utils.Path import Path
from utils.Utils import Utils
from workbench.deprecated.NGramContext import NGramContext
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
# @Ali: der Pfad kann entweder wie vorher als string oder mit dem neuen Path objekt übergeben werden
# ~ wegen kompatibilität mit PC Mac ;)
data_handler.load(str(Path(['resources', 'BPI_Challenge_2012_filtered.xes'])))
amount_req_batches = [(0, 20000, 'LOW'),
                      (20000, 40000, 'MEDIUM'),
                      (40000, 50000, 'HIGH'),
                      (50000, 1000000, 'SUPER_HIGH')]
peep_ratio_batches = [(0, 0.5, 'FEW_P'),
                      (0.5, 1, 'MANY_P')]
data_handler.process([TraceAttributeBatching(attribute_name='AMOUNT_REQ', batches=amount_req_batches),
                      AttributeEventCountRatio(attribute_name='org:resource', to_trace_attribute='PEEP_RATIO'),
                      TraceAttributeBatching(attribute_name='PEEP_RATIO', batches=peep_ratio_batches),
                      Shuffle()])

possible_event_features = [['concept:name']]
possible_trace_features = [['AMOUNT_REQ', 'PEEP_RATIO'], ['AMOUNT_REQ'], ['PEEP_RATIO'], []]


indices = random.choice((list(range(len(data_handler)))))

test_handler = data_handler[-1300:]
train_handler = data_handler[:-1300]

test_handler.process([RandomTraceCut(min_trace_length=5),
                      AttributeEventCountRatio(attribute_name='org:resource',
                                               to_trace_attribute='PEEP_RATIO'),
                      TraceAttributeBatching(attribute_name='PEEP_RATIO', batches=peep_ratio_batches)])

for event_feature in possible_event_features:
    for trace_feature in possible_trace_features:
        # TODO: For schleife mit möglichkeiten
        encoder = NGramEncoder(event_features=event_feature,
                               trace_features=trace_feature,
                               target='concept:name')

        ngram = NGramContext(n=3, encoder=encoder)
        ngram.fit(train_handler)

        ground_truth = []
        for i in range(len(test_handler)):
            ground_truth.append(test_handler.dataset.instances[i].events.pop())
        prediction = Utils.merge_ngram_probabilites(ngram.predict(test_handler))
        correctly_classified = 0
        for i in range(len(test_handler)):
            predicted_value = max(prediction[i], key=prediction[i].get)
            if ground_truth[i].identifier == predicted_value:
                correctly_classified += 1

        accuracy = correctly_classified / len(test_handler)
        print(f'{event_feature},{trace_feature}:{accuracy}')
        for i in range(len(test_handler)):
            test_handler.dataset.instances[i].events.append(ground_truth[i])
