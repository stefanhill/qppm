import logging

import numpy as np
from pennylane import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)


builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=2, normalization=True))

X, Y = builder.build(data_handler)


from sklearn.svm import SVC

# TODO: qml.kernels.kernel_matrix returns the value for the kernel
svm = SVC().fit(X, Y)


def accuracy(classifier, X, Y_target):
    return 1 - np.count_nonzero(classifier.predict(X) - Y_target) / len(Y_target)


accuracy_init = accuracy(svm, X, Y)
print(f"The accuracy of the kernel with random parameters is {accuracy_init:.3f}")
