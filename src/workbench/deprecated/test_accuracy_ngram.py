import logging

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from ml.classifier.NGram import NGram
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
data_handler.performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)
data_handler.mine_batching_patterns()

ngram = NGram(n=3)
ngram.fit(data_handler)

accuracy = ngram.evaluate(data_handler)

