import logging
from typing import TYPE_CHECKING

import numpy as np
import pennylane as qml
from pennylane import numpy as pnp, AdamOptimizer

from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from ml.MLUtils import MLUtils
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.metrics.MetricFunctions import MetricFunctions
from ml.metrics.VectorMetric import VectorMetric
from utils.Decorators import timer, memory, eval_logger
from utils.Enums import PredictionType

if TYPE_CHECKING:
    from ml.encoding.BuilderConfiguration import BuilderConfiguration


class VQC(AbstractClassifier):
    def __init__(self,
                 state_preparation=QuantumEmbedding.angle,
                 layer=QuantumLayer.default_variational,
                 optimizer=AdamOptimizer(0.05),
                 cost_fn=MetricFunctions.square_loss,
                 n_layers: int = 2,
                 n_wires: int = 2,
                 batch_size: int = 16,
                 epochs: int = 100,
                 dev=None,
                 **kwargs):

        from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
        from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder

        super().__init__(**kwargs)
        self._model = None
        self._weights = None
        self._bias = None

        self._state_preparation = state_preparation
        self._layer = layer
        self._n_layers = n_layers
        self._n_wires = n_wires
        self._batch_size = batch_size
        self._epochs = epochs

        self._dev = dev
        self._opt = optimizer
        self._cost_fn = cost_fn

        self.prediction_type = PredictionType.ACTIVITY
        if self.encoder is None:
            self.encoder = EventLogEncodingBuilder() \
                .add(IndexBasedEncoder(window=self._n_wires, normalization=True))

        self._is_trained = False

    @property
    def model(self):
        return None if not self._is_trained else self._variational_classifier

    @model.setter
    def model(self, value):
        raise NotImplementedError('Cannot set model from outer scope!')

    def _variational_classifier(self, w, x, b):

        @qml.qnode(self._dev)
        def circuit(x_, w_):
            assert len(w_) >= 2
            for i_ in range(len(w_)):
                if i_ != 0:
                    self._state_preparation(x_, n_wires=self._n_wires)
                self._layer(w_[i_])
            return tuple([qml.expval(qml.PauliZ(wires=i_)) for i_ in range(self._n_wires)])

        return circuit(x, w) + b

    @eval_logger
    def _cost(self, weights, bias, features, labels):
        predictions = [self._variational_classifier(weights, x, bias) for x in features]
        return self._cost_fn(labels, predictions)

    @timer
    @memory
    def fit(self, train_handler: 'EventLogHandler', val_handler: 'EventLogHandler' = None):
        x_train_temp, y_train_temp = self.encoder.build(train_handler)

        # Determine number of wires needed, next higher integer to the power of two for y, min number of features
        min_wires_x = len(x_train_temp[0])
        min_wires_y = MLUtils.next_2_pot(len(train_handler.process_definition.activity_to_id.keys()))
        self._n_wires = max(min_wires_x, min_wires_y)

        # Make features and labels differentiable
        x_train = pnp.array(x_train_temp, requires_grad=False)
        y_train = pnp.zeros(shape=(len(y_train_temp), self._n_wires), requires_grad=False)

        # Transform labels to encoding that matches the quantum computer with respective number of wires
        for i in range(len(y_train_temp)):
            y_train[i] = MLUtils.float_to_binary_array(y_train_temp[i], self._n_wires)

        # Automatically set device using determined number of wires
        if self._dev is None:
            self._dev = qml.device("default.qubit", wires=self._n_wires)

        # Initialise weights and bias if not trained before, classifier can be retrained
        if self._weights is None:
            self._weights = 0.01 * pnp.random.randn(self._n_layers, self._n_wires, 3, requires_grad=True)
        if self._bias is None:
            self._bias = pnp.zeros(self._n_wires, requires_grad=True)

        # Iterate for some epochs and optimize the variational model
        n_train_samples = len(x_train)
        for i_ in range(self._epochs):
            # Create a random batch of samples
            batch_index = pnp.random.randint(0, n_train_samples, (self._batch_size,))
            x_train_batch = x_train[batch_index]
            y_train_batch = y_train[batch_index]

            # Optimize weights and bias
            self._weights, self._bias, _, _ = self._opt.step(self._cost, self._weights, self._bias, x_train_batch,
                                                             y_train_batch)
            logging.info(f'Training VQE -> Epoch:{i_}')

        self._is_trained = True

    @timer
    @memory
    def predict(self, predict_handler: 'EventLogHandler',
                builder_configuration: 'BuilderConfiguration' = None) -> 'AbstractPrediction':
        x_predict, _ = self.encoder.build(predict_handler, builder_configuration=builder_configuration, predict=True)

        prediction = [self._variational_classifier(self._weights, x, self._bias) for x in x_predict]
        return np.array([MLUtils.binary_array_argmax(p) for p in prediction])

    def evaluate(self, val_handler: 'EventLogHandler') -> 'AbstractMetric':
        x_val, y_val = self.encoder.build(val_handler)

        prediction = [self._variational_classifier(self._weights, x, self._bias) for x in x_val]
        y_pred = np.array([MLUtils.binary_array_argmax(p) for p in prediction])

        return VectorMetric(y_val, y_pred)
