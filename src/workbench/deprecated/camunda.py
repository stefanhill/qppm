import pycamunda.processinst

url = 'http://localhost:8080/engine-rest'

get_instances = pycamunda.processinst.GetList(url, process_definition_key='CoronaSupermarkt')
instances = get_instances()

for instance in instances:
    print('Process instance id:', instance.id_)