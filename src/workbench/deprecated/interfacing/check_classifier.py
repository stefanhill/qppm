import sys

from sklearn.model_selection import cross_validate

sys.path.append('/home/david/masterthesis-quantum-ppm/masterthesis/src/')

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.QKE import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4))

X, y = builder.build(handler)

# acc_ngram = cross_validate(NGram(max_classes=len(handler.process_definition)), X, y, cv=3)
# acc_rf = cross_validate(RandomForestClassifier(max_depth=4, random_state=0), X, y, cv=3)
# acc_xgboost = cross_validate(XGBClassifier(), X, y, cv=3)
#acc_qke = cross_validate(QKE(n_layers=2, kernel=QuantumEmbedding.zz, use_jax=True, debug=True), X, y, cv=3)
#print(acc_qke)
acc_qke_zz = cross_validate(QKE(n_layers=1, kernel=QuantumEmbedding.zz, use_jax=True, debug=True), X, y, cv=3)
print(acc_qke_zz)
acc_qke_deep_zz = cross_validate(QKE(n_layers=1, kernel=QuantumEmbedding.deep_zz, use_jax=True, debug=True), X, y, cv=3)
print(acc_qke_deep_zz)
acc_qke_zz_angle = cross_validate(QKE(n_layers=1, kernel=QuantumEmbedding.zz_angle, use_jax=True, debug=True), X, y, cv=3)
print(acc_qke_zz_angle)
