import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.QKE import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})

encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True))

embeddings = [QuantumEmbedding.angle, QuantumEmbedding.zz]

accuracies = []
for embedding in embeddings:
    qke = QKE(n_layers=2, kernel=embedding, encoder=encoder)
    qke.fit(data_handler)
    metric_qke = qke.evaluate(data_handler)
    accuracies.append(metric_qke.accuracy())
