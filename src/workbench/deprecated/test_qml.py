from pennylane import device, qnode, PauliZ, expval, Hadamard, RY, GradientDescentOptimizer

"""
dev = device('default.qubit', wires=2, shots=1)
U = np.array([[0., -0.70710678, 0., 0.70710678],
              [0.70710678, 0., -0.70710678, 0.],
              [0.70710678, 0., 0.70710678, 0.],
              [0., -0.70710678, 0., -0.70710678]])


@qnode(dev)
def circuit():
    QubitUnitary(U, wires=[0, 1])
    # return probs(wires=[0, 1])
    # return sample(PauliZ(wires=0)), sample(PauliZ(wires=1))
    return expval(PauliZ(wires=0)), expval(PauliZ(wires=1))


print(circuit())
print(dev.state)

"""

dev = device('default.qubit', wires=1)


@qnode(dev)
def circuit(phi_):
    Hadamard(wires=0)
    RY(phi_, wires=0)
    return expval(PauliZ(wires=0))


phi = 0.2
opt = GradientDescentOptimizer(stepsize=0.2)

for i in range(5):
    phi = opt.step(circuit, phi)
    print(phi)
