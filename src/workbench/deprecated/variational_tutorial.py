# https://pennylane.ai/qml/demos/tutorial_variational_classifier.html

import pennylane as qml
from pennylane import numpy as np
from pennylane.optimize import NesterovMomentumOptimizer

# device with 2^4=16 dimensional hilbert space
dev = qml.device("default.qubit", wires=4)

# layer of the variational circuit
def layer(W):
    # TODO: Rot takes phi, theta and omega as inputs
    qml.Rot(W[0, 0], W[0, 1], W[0, 2], wires=0)
    qml.Rot(W[1, 0], W[1, 1], W[1, 2], wires=1)
    qml.Rot(W[2, 0], W[2, 1], W[2, 2], wires=2)
    qml.Rot(W[3, 0], W[3, 1], W[3, 2], wires=3)

    # entagles all qubits
    # we could also use qml.broadcast with the ring shape here
    qml.CNOT(wires=[0, 1])
    qml.CNOT(wires=[1, 2])
    qml.CNOT(wires=[2, 3])
    qml.CNOT(wires=[3, 0])


def statepreparation(x):
    # TODO: basis state is a binary encoding which sets all qubits equal to their amplitude
    qml.BasisState(x, wires=[0, 1, 2, 3])


@qml.qnode(dev)
def circuit(weights, x):
    statepreparation(x)

    for W in weights:
        # TODO: weight shape?
        layer(W)

    # TODO: measure on the z axis
    return qml.expval(qml.PauliZ(0))


# TODO: circuit seems to be treated like a perceptron
def variational_classifier(weights, bias, x):
    return circuit(weights, x) + bias


# just the normal loss function
def square_loss(labels, predictions):
    loss = 0
    for l, p in zip(labels, predictions):
        loss = loss + (l - p) ** 2

    loss = loss / len(labels)
    return loss

# just the normal accuracy function
def accuracy(labels, predictions):
    loss = 0
    for l, p in zip(labels, predictions):
        if abs(l - p) < 1e-5:
            loss = loss + 1
    loss = loss / len(labels)

    return loss

# calculates the loss function for a prediction on X and Y
def cost(weights, bias, X, Y):
    predictions = [variational_classifier(weights, bias, x) for x in X]
    return square_loss(Y, predictions)

# load the test dataset
data = np.loadtxt("C:\\git\\masterthesis\\resources\\parity.txt")
# all without last column
X = np.array(data[:, :-1], requires_grad=False)
# only last column
Y = np.array(data[:, -1], requires_grad=False)
Y = Y * 2 - np.ones(len(Y))  # shift label from {0, 1} to {-1, 1}


for i in range(5):
    print("X = {}, Y = {: d}".format(X[i], int(Y[i])))

print("...")

# weights are initialized randomly
np.random.seed(0)
num_qubits = 4
num_layers = 2
# initialization of the layers and the weights
weights_init = 0.01 * np.random.randn(num_layers, num_qubits, 3, requires_grad=True)
bias_init = np.array(0.0, requires_grad=True)

print(weights_init, bias_init)

opt = NesterovMomentumOptimizer(0.5)
batch_size = 5

weights = weights_init
bias = bias_init
for it in range(25):
    # Update the weights by one optimizer step
    batch_index = np.random.randint(0, len(X), (batch_size,))
    X_batch = X[batch_index]
    Y_batch = Y[batch_index]
    # one step of the optimizer, takes cost, weights, bias and the values as inputs
    weights, bias, _, _ = opt.step(cost, weights, bias, X_batch, Y_batch)

    # Compute accuracy
    predictions = [np.sign(variational_classifier(weights, bias, x)) for x in X]
    acc = accuracy(Y, predictions)

    print(
        "Iter: {:5d} | Cost: {:0.7f} | Accuracy: {:0.7f} ".format(
            it + 1, cost(weights, bias, X, Y), acc
        )
    )
