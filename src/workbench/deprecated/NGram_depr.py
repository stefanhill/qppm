import typing
from functools import reduce
from typing import TYPE_CHECKING

import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from ml.metrics.ProbabilisticVectorMetric import ProbabilisticVectorMetric
from utils.Decorators import timer, memory
from utils.Enums import PredictionType

if TYPE_CHECKING:
    from ml.encoding.BuilderConfiguration import BuilderConfiguration


class NGramOld(AbstractClassifier):

    def __init__(self, n: int = 2, feature_delimiter: 'str' = '$', **kwargs):

        """N grams with fixed n

        Args:
            n:
            feature_delimiter:
            **kwargs:
        """

        from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
        from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder

        super().__init__(**kwargs)
        self._n = n
        self.model: typing.Dict[str, typing.Dict[str, float]] = {}
        self._feature_delimiter = feature_delimiter

        self.prediction_type = PredictionType.ACTIVITY
        if self.encoder is None:
            self.encoder = EventLogEncodingBuilder() \
                .add(IndexBasedEncoder(window=self._n))

    def _feature_to_string(self, feature: np.ndarray):
        return reduce(lambda a, b: f'{a}{self._feature_delimiter}{b}', feature)

    @timer
    @memory
    def fit(self, train_handler: 'EventLogHandler', val_handler: 'EventLogHandler' = None):

        self.model = {}

        x_train, y_train = self.encoder.build(train_handler)

        assert len(set(y_train)) <= len(train_handler.process_definition)

        for feature, label in zip(x_train, y_train):
            prefix = self._feature_to_string(feature)
            if prefix not in self.model.keys():
                self.model[prefix] = np.zeros(len(train_handler.process_definition))
            self.model[prefix][int(label)] += 1

        for prefix in self.model.keys():
            total = np.sum(self.model[prefix])
            if total > 0:
                self.model[prefix] /= total

    @timer
    @memory
    def predict(self, predict_handler: 'EventLogHandler',
                builder_configuration: 'BuilderConfiguration' = None) -> 'AbstractPrediction':
        prediction = []

        x_predict, _ = self.encoder.build(predict_handler, builder_configuration=builder_configuration, predict=True)

        for feature in x_predict:
            prefix = self._feature_to_string(feature)
            if prefix in self.model:
                prediction.append(self.model[prefix])
            else:
                # TODO: how to handle errors here
                pass

        return np.array(prediction)

    def evaluate(self, val_handler: 'EventLogHandler') -> 'AbstractMetric':
        predictions = []

        x_val, y_val = self.encoder.build(val_handler)

        for feature in x_val:
            prefix = self._feature_to_string(feature)
            if prefix in self.model:
                predictions.append(self.model[prefix])
            else:
                predictions.append(np.zeros(len(self.model.get(0))) - 1.0)

        return ProbabilisticVectorMetric(y_val, np.array(predictions))
