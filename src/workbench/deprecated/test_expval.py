import pennylane as qml



dev = qml.device('default.qubit', wires=4, shots=100)


@qml.qnode(dev)
def circuit():
    qml.Hadamard(wires=0)
    qml.Hadamard(wires=1)
    qml.Hadamard(wires=2)
    qml.Hadamard(wires=3)
    return qml.expval()

print(circuit())