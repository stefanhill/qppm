import logging

from data.handler.EventLogHandler import EventLogHandler
from data.processing.filter.RandomTraces import RandomTraces
from data.processing.preprocessing.RandomTraceCut import RandomTraceCut
from utils.Path import Path
from workbench.deprecated.NGramContext import NGramContext
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder

logging.basicConfig(level=logging.DEBUG)

data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', 'BPI_Challenge_2012_filtered.xes'])))

encoder = NGramEncoder(event_features=['concept:name'],
                       trace_features=['AMOUNT_REQ'],
                       target='concept:name')

ngram = NGramContext(n=3, encoder=encoder)
ngram.fit(data_handler)


running_instance = data_handler.get(RandomTraces(no_traces=1, min_trace_length=10))
running_instance.process([RandomTraceCut(min_trace_length=5)])

prediction = ngram.predict(running_instance)
