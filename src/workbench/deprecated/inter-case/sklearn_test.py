import logging

from ml.classifier.SKWrapper import SKWrapper
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier

from data.handler.EventLogHandler import EventLogHandler
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})

# TODO: currently, number of instances is cut off inside qke.fit(), implement methods for train test split and sampling

encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True))

encoder.build(data_handler)

rf = SKWrapper(model=RandomForestClassifier(max_depth=2, random_state=0), encoder=encoder)
rf.fit(data_handler)
metric_rf = rf.evaluate(data_handler)

xgb = SKWrapper(model=XGBClassifier(), encoder=encoder)
xgb.fit(data_handler)
metric_xgb = xgb.evaluate(data_handler)

print(metric_rf.accuracy())
print(metric_xgb.accuracy())
