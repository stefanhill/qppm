import copy
import datetime
import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.FutureBatchingBehaviour import FutureBatchingBehaviour
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from ml.encoding.intercase.TopBusyResource import TopBusyResource
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)
metrics = {}

train_handler = EventLogHandler()
train_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

val_handler = EventLogHandler()
val_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_validation.xes'])))

# Ensure the mapping is the same for train and validation handler
val_handler.process_definition = train_handler.process_definition

train_handler.mine_batching_patterns()
val_handler.mine_batching_patterns()

encoders = {}

ngram = NGram(n=4)
ngram.fit(train_handler)

timedelta = datetime.timedelta(weeks=8)

def autonormalize_encoder(enc, *datasets):
    builder_ = EventLogEncodingBuilder() \
        .add(enc)

    minimum = 0
    maximum = 1

    for dataset in datasets:
        x_, _ = builder_.build(dataset)
        minimum = min(minimum, min(x_))
        maximum = max(maximum, max(x_))

    temp_enc = copy.deepcopy(enc)
    temp_enc._normalization = (float(minimum), float(maximum))
    return temp_enc


# (0.0, 19.0)
normalized_peer_cases = autonormalize_encoder(NoPeerCases(timeframe=timedelta), train_handler, val_handler)

# (0.0, 260.0)
normalized_pac = autonormalize_encoder(PeerActivityCount(timeframe=timedelta), train_handler, val_handler)

# (0.0, 4.0)
normalized_resource_count = autonormalize_encoder(ResourceCount(timeframe=timedelta), train_handler, val_handler)

# (-5918400.0, 550800.0)
normalized_average_delay = autonormalize_encoder(AverageDelay(timeframe=timedelta), train_handler, val_handler)

builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True)) \
    .add(normalized_peer_cases) \
    .add(normalized_pac) \
    .add(FrequentPreviousActivity(timeframe=timedelta, no_activities=2, normalization=True)) \
    .add(normalized_resource_count) \
    .add(TopBusyResource(timeframe=timedelta, no_resources=2, normalization=True)) \
    .add(FutureBatchingBehaviour(segment_classifier=ngram, include_average_times=False)) \
    .add(normalized_average_delay)

features, labels = builder.build(train_handler)
