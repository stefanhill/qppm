import datetime
import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.FutureBatchingBehaviour import FutureBatchingBehaviour
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from ml.encoding.intercase.TopBusyResource import TopBusyResource
from ml.encoding.intracase.AggregationEncoder import AggregationEncoder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})
data_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))

ngram = NGram(n=3)
ngram.fit(data_handler)

timedelta = datetime.timedelta(days=1)

builder = EventLogEncodingBuilder() \
    .add(AggregationEncoder()) \
    .add(IndexBasedEncoder()) \
    .add(NoPeerCases(timeframe=timedelta)) \
    .add(PeerActivityCount(timeframe=timedelta)) \
    .add(FrequentPreviousActivity(timeframe=timedelta, no_activities=2)) \
    .add(ResourceCount(timeframe=timedelta)) \
    .add(TopBusyResource(timeframe=timedelta, no_resources=2)) \
    .add(FutureBatchingBehaviour(segment_classifier=ngram, include_average_times=True)) \
    .add(AverageDelay(timeframe=timedelta))

features, labels = builder.build(data_handler)
pass
