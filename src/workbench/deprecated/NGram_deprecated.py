import typing

from deprecation import deprecated

from data.dataset.EndEvent import EndEvent
from data.dataset.NoneEvent import NoneEvent
from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from utils.Decorators import timer, memory
from utils.Path import Path
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder
from workbench.deprecated.deprecated.Tokenizer import Tokenizer


@deprecated
class NGram_deprecated(AbstractClassifier):

    def __init__(self, n: int = 2, **kwargs):
        super().__init__(**kwargs)
        self._n = n
        self._gram_to_probability: typing.Dict[int, typing.Dict[str, typing.Dict[str, float]]] = {}

    @property
    def encoder(self) -> 'NGramEncoder':
        return self._encoder

    @encoder.setter
    def encoder(self, value: 'NGramEncoder'):
        self._encoder = value
    
    @timer
    @memory
    def fit(self, data: 'EventLogHandler', evaluation_data: 'EventLogHandler' = None):
        for n_ in range(1, self._n + 1):
            tokens = []
            for trace in data.dataset.traces:
                tokens.extend(Tokenizer(token_size=n_ + 1).tokenize(trace))
            grams = NGramEncoder().encode(tokens)
            prefix_to_outcome = {k: [] for k, _ in grams}
            for k, v in grams:
                prefix_to_outcome[k].append(v)
            self._gram_to_probability[n_] = {}
            for prefix, labels in prefix_to_outcome.items():
                self._gram_to_probability[n_][prefix] = {}
                for label in set(labels):
                    self._gram_to_probability[n_][prefix][label] = labels.count(label) / len(labels)

    @timer
    @memory
    def predict(self, data: 'EventLogHandler') -> 'AbstractPrediction':
        prediction = [{} for _ in range(len(data))]
        for n_ in range(1, self._n + 1):
            tokens = []
            for trace in data.dataset.traces:
                tokens.extend(Tokenizer(token_size=n_, start_index=-1).tokenize(trace))
            grams = NGramEncoder(predict=True).encode(tokens)
            for i, (prefix, _) in enumerate(grams):
                if not (isinstance(tokens[i].events[0], NoneEvent) or
                        isinstance(tokens[i].events[-1], EndEvent)):
                    prediction[i][prefix] = self._gram_to_probability[n_][prefix]
                else:
                    continue
        return prediction

    def evaluate(self, data: 'EventLogHandler') -> 'AbstractMetric':
        pass

    def save(self, path: typing.Union[str, 'Path']):
        pass

    def load_weights(self, path: typing.Union[str, 'Path']):
        pass
