import datetime
import logging

from data.handler.EventLogHandler import EventLogHandler
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)


filenames = ['bpi2011_train.xes', 'bpi2011_validation.xes', 'bpi2017_train.xes', 'bpi2017_validation.xes', 'rtfm_train.xes', 'rtfm_validation.xes']

for filename in filenames:
    data_handler = EventLogHandler()
    data_handler.load(str(Path(['resources', 'test_logs', filename])))
    data_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(days=5))

