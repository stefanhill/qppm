import os

from sklearn.base import ClassifierMixin

from api.fun.mappings import APIUtils
from data.DataUtils import DataUtils
from data.handler.EventLogHandler import EventLogHandler
from ml.encoding.BuilderConfiguration import BuilderConfiguration
from utils.Utils import Utils

CLASSIFIER_FOLDER = 'C:\\git\\masterthesis\\appdata\\classifier'

request_data = {'given_name': 'qke1$0',
                'classifier': 'QKE'}
given_name = request_data['given_name']
classifier_path = os.path.join(CLASSIFIER_FOLDER, f'{given_name}.classifier')
predict_path = 'C:\\git\\masterthesis\\appdata\\temp\\qke1$training.xes'

data_handler = EventLogHandler()
data_handler.load(predict_path)

median_case_length = DataUtils.determine_median_case_length(data_handler)

builder, scaler = APIUtils.load_encoding_pipeline(CLASSIFIER_FOLDER, given_name)

classifier: 'ClassifierMixin'
if f'{given_name}.classifier' in os.listdir(CLASSIFIER_FOLDER):
    classifier = Utils.load_classifier(classifier_path)
else:
    raise NameError('Classifier does not exist!')

builder_configuration = BuilderConfiguration([0], [data_handler.dataset.get_trace_by_id(0).events[-1].identifier])

X, _ = builder.build(data_handler, builder_configuration=builder_configuration, predict=True)
X = scaler.transform(X)

prediction = classifier.predict_proba(X)
